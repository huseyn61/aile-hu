#ifndef AHU_SEVKISLEMLERI_H
#define AHU_SEVKISLEMLERI_H

#include <QDialog>

namespace Ui {
class ahu_SevkIslemleri;
}

class ahu_SevkIslemleri : public QDialog
{
    Q_OBJECT

public:
    explicit ahu_SevkIslemleri(QWidget *parent = 0);
    ~ahu_SevkIslemleri();

private:
    Ui::ahu_SevkIslemleri *ui;
};

#endif // AHU_SEVKISLEMLERI_H
