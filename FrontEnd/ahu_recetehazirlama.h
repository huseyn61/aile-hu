#ifndef AHU_RECETEHAZIRLAMA_H
#define AHU_RECETEHAZIRLAMA_H

#include <QDialog>

namespace Ui {
class ahu_ReceteHazirlama;
}

class ahu_ReceteHazirlama : public QDialog
{
    Q_OBJECT

public:
    explicit ahu_ReceteHazirlama(QWidget *parent = 0);
    ~ahu_ReceteHazirlama();

private:
    Ui::ahu_ReceteHazirlama *ui;
};

#endif // AHU_RECETEHAZIRLAMA_H
