#ifndef AHU_ANAFORM_H
#define AHU_ANAFORM_H

#include <QWidget>

namespace Ui {
class ahu_AnaForm;
}

class ahu_AnaForm : public QWidget
{
    Q_OBJECT

public:
    explicit ahu_AnaForm(QWidget *parent = 0);
    ~ahu_AnaForm();

private:
    Ui::ahu_AnaForm *ui;
};

#endif // AHU_ANAFORM_H
