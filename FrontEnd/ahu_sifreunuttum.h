#ifndef AHU_SIFREUNUTTUM_H
#define AHU_SIFREUNUTTUM_H

#include <QDialog>

namespace Ui {
class ahu_sifreunuttum;
}

class ahu_sifreunuttum : public QDialog
{
    Q_OBJECT

public:
    explicit ahu_sifreunuttum(QWidget *parent = 0);
    ~ahu_sifreunuttum();

private:
    Ui::ahu_sifreunuttum *ui;
};

#endif // AHU_SIFREUNUTTUM_H
