#ifndef AHU_H
#define AHU_H

#include <QMainWindow>

namespace Ui {
class AHU;
}

class AHU : public QMainWindow
{
    Q_OBJECT

public:
    explicit AHU(QWidget *parent = 0);
    ~AHU();

private:
    Ui::AHU *ui;
};

#endif // AHU_H
